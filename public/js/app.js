const backend_path = "$.php";
const container = $("#container");
var ajaxReqDt = false, ajaxReqPhp = false;

function addProduct(form){
    var na = $("#noti-area"); na.hide();
    $.post(backend_path.replace("$", form.data("action")), form.serialize(), function (data) {
    })
    .done(function(data){
        form[0].reset();
        na.html(data);
    })
    .always(function(){
        na.show();
    })
    .fail(function() {
        na.html("An unexpected error has occurred.")
    });
}

function getPage(endpoint){
    $.get(backend_path.replace("$", endpoint), function(data){
       container.html(data);
    });
}

function productRelationship(productIdTo, del){
    var fn = del ? "deleteProductRelationship" : "insertProductRelationship";
    $.getJSON(backend_path.replace("$", "ajax"), {fn:fn, payload:{productIdFrom:$("#product_id").val(), productIdTo:productIdTo}})
        .done(function( data ) {
            if(data.error) {
                alert(data.error);
            }
            searchProducts($("#customer_id").val(), $("#search").val(), 1);
        });
}

function searchProducts(customerId, name, page){
    var productId = $("#product_id").val();
    var container = $("#cp-container");
    if (!ajaxReqDt){
        ajaxReqDt = true;
        $.getJSON("ajax.php", {fn:"searchProductsByName", payload:{customer_id:customerId, name:name, product_id:productId}}, function (data) {
            if(data.success) {
                $("#dt-results").show();
                var dt = $("#table-dt").DataTable();

                dt.clear();
                dt.rows.add(data.items).draw();
            }
        }).always(function(){
            ajaxReqDt = false;
        });
        container.show();
    }

    if(!ajaxReqPhp) {
        ajaxReqPhp = true;
        $.get(backend_path.replace("$", "searchProducts") + "?customer_id=" + customerId + "&name=" + name + "&page=" + page + "&product_id=" + productId, function (data) {
            $("#results").html(data);
            $("button[data-id]").one("click", function () {
                $(this).html("Work in progress...")
                $(this).prop("disabled", true);
                productRelationship($(this).data("id"), $(this).data("del"));
            });
        }).always(function(){
            ajaxReqPhp = false
        })
        container.show();
    }
}

function refreshProducts(customerId){
    var $select = $('#product_id').empty().prop("disabled", true);
    var $search = $('#search').val("").prop("disabled", true);
    $.getJSON(backend_path.replace("$", "ajax"), {fn:"query", payload:{model:"product", payload:{customer_id:customerId}}})
        .done(function( data ) {
            if(data.success) {
                var e = $('<option/>', {value: ""}).text("Select a product...").prop('selected', true);
                e.appendTo($select);
                $.each(data.items, function (i, item) {
                    var o = $('<option/>', {value: item.id}).text(item.name);
                    o.appendTo($select);
                });
                $select.prop("disabled", false);
            }
        });
}

(function() {
    'use strict';
    feather.replace();

    $("a[data-link]").on("click", function(){
        getPage($(this).data("link"));
    });

})();


