<?php

require "../autoload.php";

use Minderest\SkillTest\DBHelper as DBHelper;

function insertProductRelationship($args){
    $productIdFrom = $args['productIdFrom'];
    $productIdTo = $args['productIdTo'];
    $em = DBHelper::getInstance();
    $toReturn = [];
    try {
        $em->insertProductRelationship($productIdFrom, $productIdTo);
        $toReturn['success'] = true;
    } catch (Exception $e){
        $toReturn["error"] = $e->getMessage();
    }

    return $toReturn;
}

function deleteProductRelationship($args){
    $productIdFrom = $args['productIdFrom'];
    $productIdTo = $args['productIdTo'];
    $em = DBHelper::getInstance();
    $toReturn = [];
    try {
        $em->deleteProductRelationship($productIdFrom, $productIdTo);
        $toReturn['success'] = true;
    } catch (Exception $e){
        $toReturn["error"] = $e->getMessage();
    }

    return $toReturn;
}

function searchProductsByName($args){
    $em = DBHelper::getInstance();

    try {
        $products = $em->searchProductByName($args['customer_id'], $args['name'], 0, 0);

        $em->close();

        return ['success' => true, 'items' => $products];

    } catch (Exception $e){
        return ['success' => false, 'error' => $e->getMessage()];
    }
}

function query($args){
    $model = $args['model'];
    $payload = $args['payload'];
    $em = DBHelper::getInstance();

    $where = "1=1";
    if(is_array($payload)){
        foreach($payload as $key => $value){
            $where .= " AND " . $key;
            if (is_array($value)) $where .= " IN (" . implode(",", $value).")";
            else if(is_numeric($value)) $where .= "=" . $value;
            else if(preg_match('/=|!=|>=|<=|<|>/i', $value)) $where .= $value;
            else $where .= " LIKE '%" . $value ."%'";
        }
    }

    try {
        $result = $em->query($model, $where);
        $em->close();

        return ['success' => true, 'items' => $result];

    } catch (Exception $e){
        return ['success' => false, 'error' => $e->getMessage()];
    }

}

$fn = $_GET['fn'];
$payload = $_GET['payload'];

$result = $fn($payload);
echo json_encode($result);

