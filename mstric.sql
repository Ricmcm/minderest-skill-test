/* -------------- MINDTEREST SKILL TEST SQL SCRIPT START -------------- */

                CREATE DATABASE IF NOT EXISTS `minderest`;
                USE `minderest`;

                DROP TABLE IF EXISTS `customer`;
                CREATE TABLE  `customer` (
                          `id` INT(11) UNSIGNED AUTO_INCREMENT,
                          `name` VARCHAR(150) NOT NULL,
                          `code` VARCHAR(25) UNIQUE NOT NULL,
                          PRIMARY KEY (`id`) )
                        ENGINE = INNODB DEFAULT CHARSET=utf8;

                DROP TABLE IF EXISTS `product`;
                CREATE TABLE `product` (
                          `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                          `name` VARCHAR(255) NOT NULL,
                          `customer_id` INT(11) UNSIGNED NOT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `UNIQUE` (`name`,`customer_id`),
                          KEY `FK_CUSTOMER_ID` (`customer_id`),
                          CONSTRAINT `FK_CUSTOMER_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=INNODB DEFAULT CHARSET=utf8;

                DROP TABLE IF EXISTS `product_relationship`;
                CREATE TABLE `product_relationship` (
                        `product_id_from` INT(11) UNSIGNED NOT NULL,
                        `product_id_to` INT(11) UNSIGNED NOT NULL,
                        UNIQUE KEY `UNIQUE_INDEX` (`product_id_from`,`product_id_to`), KEY `FK_PRODUCT_ID_TO` (`product_id_to`),
                        CONSTRAINT `FK_PRODUCT_ID_FROM` FOREIGN KEY (`product_id_from`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                        CONSTRAINT `FK_PRODUCT_ID_TO` FOREIGN KEY (`product_id_to`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=INNODB DEFAULT CHARSET=utf8;
DELIMITER $$

                DROP TRIGGER IF EXISTS `ins_product_relationship`$$

                CREATE
                    TRIGGER `ins_product_relationship` BEFORE INSERT ON `product_relationship`
                    FOR EACH ROW
                    BEGIN
                        IF(new.product_id_from = new.product_id_to) THEN
                            SIGNAL SQLSTATE '45000'
                            SET MESSAGE_TEXT = 'Is already same product';
                        END IF;
                        SELECT MAX(customer_id) INTO @customer_id_from FROM product WHERE id=new.product_id_from LIMIT 1;
                        SELECT MAX(customer_id) INTO @customer_id_to FROM product WHERE id=new.product_id_to LIMIT 1;
                        IF(@customer_id_from = @customer_id_to) THEN
                            SIGNAL SQLSTATE '45000'
                            SET MESSAGE_TEXT = 'Same client for product relationship is not allowed';
                        END IF;
                    END;
$$
DELIMITER ;
INSERT INTO `customer` (`name`, `code`) VALUES ("CUSTOMER 0", "customer-0");
INSERT INTO `customer` (`name`, `code`) VALUES ("CUSTOMER 1", "customer-1");
INSERT INTO `customer` (`name`, `code`) VALUES ("CUSTOMER 2", "customer-2");
INSERT INTO `customer` (`name`, `code`) VALUES ("CUSTOMER 3", "customer-3");
INSERT INTO `customer` (`name`, `code`) VALUES ("CUSTOMER 4", "customer-4");
INSERT INTO `customer` (`name`, `code`) VALUES ("CUSTOMER 5", "customer-5");
INSERT INTO `customer` (`name`, `code`) VALUES ("CUSTOMER 6", "customer-6");
INSERT INTO `customer` (`name`, `code`) VALUES ("CUSTOMER 7", "customer-7");
INSERT INTO `customer` (`name`, `code`) VALUES ("CUSTOMER 8", "customer-8");
INSERT INTO `customer` (`name`, `code`) VALUES ("CUSTOMER 9", "customer-9");
/* -------------- MINDTEREST SKILL TEST SQL SCRIPT END -------------- */

