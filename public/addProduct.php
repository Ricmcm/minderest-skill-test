<?php

require "../autoload.php";
use Minderest\SkillTest\DBHelper as DBHelper;

if($_SERVER['REQUEST_METHOD'] === "POST" ){
    $em = DBHelper::getInstance();
    $customerId = $_POST['product']['customer_id'];
    $name = $_POST['product']['name'];

    try {
        $em->insertProduct($customerId, $name);

        echo "<div class='alert alert-success' role='alert'>Product added successfully.</div>";
    } catch (Exception $e){
        echo "<div class='alert alert-danger' role='alert'>" . $e->getMessage() . "</div>";
    }

} else {
    $em = DBHelper::getInstance();
    $customers = $em->findAllCustomers();
    $em->close();

    ?>

<h4>Add Product</h4>

<form action="#" method="POST" data-action="addProduct" class="needs-validation" novalidate>
    <div class="row">
        <div class="col-md-3 mb-3">
            <label for="country">Customer</label>
            <select name="product[customer_id]" class="custom-select d-block w-100" id="customer_id" required="">
                <option value="">Choose...</option>
                <?php foreach($customers as $id => $customer){ ?>
                    <option value="<?php echo $id; ?>"><?php echo $customer; ?></option>
                <?php } ?>
            </select>
            <div class="invalid-feedback">
                Please select a valid customer.
            </div>
        </div>
        <div class="col-md-3 mb-3">
            <label for="name">Product Name</label>
            <input name="product[name]" type="text" class="form-control" id="name" placeholder="" value="" required="">
            <div class="invalid-feedback">
                Valid product name is required.
            </div>
        </div>
        <div class="col-md-2 mt-4">
            <label>&nbsp;</label>
            <button class="btn btn-primary mt-1" type="submit">Add</button>
        </div>
    </div>
</form>

<div id="noti-area" class="row">
</div>

<script>
    $("#noti-area").hide();

    $('form').submit(function(e) {
        e.preventDefault();
        e.stopPropagation();
        var form = $(this)[0];
        if (form.checkValidity() === false) {
            form.classList.add('was-validated');
        } else {
            form.classList.remove('was-validated');
            addProduct($(this));
        }
    });
</script>

<?php } ?>