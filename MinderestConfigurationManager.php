<?php


namespace Minderest\SkillTest;

// TODO: Load configuration from file
class MinderestConfigurationManager
{
    // Default database connection configuration
    private $host = "localhost";
    private $user = "root";
    private $password = "desarrollo";
    private $database = "minderest";

    /**
     * @var
     */
    private static $instance;

    /**
     * @return MinderestConfigurationManager
     */
    public static function getInstance()
    {
        if (is_null(self::$instance)){
            $configuration = parse_ini_file("config.ini");

            self::$instance = new MinderestConfigurationManager();
            self::$instance->setDatabase($configuration['database']);
            self::$instance->setHost($configuration['host']);
            self::$instance->setUser($configuration['user']);
            self::$instance->setPassword($configuration['password']);
        }

        return self::$instance;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param string $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getDatabase()
    {
        return $this->database;
    }

    /**
     * @param string $database
     */
    public function setDatabase($database)
    {
        $this->database = $database;
    }
}