<?php

require "./autoload.php";
use Minderest\SkillTest\DBHelper as DBHelper;
use Minderest\SkillTest\Utils as Utils;

function showMenu(){
    echo PHP_EOL . "Minderest SkillTest Utils" . PHP_EOL;
    echo "===========================" . PHP_EOL;
    echo "[1] - Load test customers" . PHP_EOL;
    echo "[2] - Generate SQL Script" . PHP_EOL;
    echo "[0] - Exit" . PHP_EOL;
}

function init(){
    $N = readline("Collection customers size: ");
    if (!is_numeric($N)){
        echo "Incorrect input " . $N . PHP_EOL;
        return;
    }

    echo "Generating " . $N . " customers." . PHP_EOL;

    return $N;
}

function insertCustomers($n){
    $em = DBHelper::getInstance();
    for($i=0;$i<$n;$i++){
        $name = "CLIENTE " . $i;
        $code = Utils::slugify($name);

        $em->newCustomer($name, $code);

        echo "[" . $i . "] " . $name . " (" . $code . ") generated." . PHP_EOL;
    }

    $em->close();
}

function getSqlScript($n){
    $output = "/* -------------- MINDTEREST SKILL TEST SQL SCRIPT START -------------- */" . PHP_EOL;
    $output .= DBHelper::getSqlScript();
    for($i=0;$i<$n;$i++){
        $name = "CUSTOMER " . $i;
        $code = Utils::slugify($name);

        $output .= "INSERT INTO `customer` (`name`, `code`) VALUES (\"" . $name . "\", \"" . $code . "\");" . PHP_EOL;
    }
    $output .= "/* -------------- MINDTEREST SKILL TEST SQL SCRIPT END -------------- */" . PHP_EOL;
    return $output;
}

$exit = false;
while(! $exit){
    showMenu();
    $option = readline("Option: ");
    switch($option){
        case "0": $exit = true; break;
        case "1": $n = init(); insertCustomers($n); break;
        case "2": $n = init(); echo getSqlScript($n) . PHP_EOL; break;
    }
}

echo "Bye :)" . PHP_EOL;

