<?php

require "../autoload.php";
use Minderest\SkillTest\DBHelper as DBHelper;

$em = DBHelper::getInstance();
$customerId = $_GET['customer_id'];
$name = $_GET['name'];
$productId = $_GET['product_id'];
$limit = 25;
$offset = ($_GET['page']-1) * $limit;

$products = $em->searchProductByName($customerId, $name, $offset, $limit);

?>

<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Product Name</th>
            <th scope="col">Customer</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
    <?php foreach($products as $product){ ?>
        <?php  $same = false; foreach(explode(",", $product['p_product_id']) as $p){
                    if($productId == $p){ $same = true; break;}
                } ?>
        <tr <?php if($same) echo "class='tr-success'"; ?>>
            <th scope="row" width="15%"><?php echo $product['p_id']; ?></th>
            <td width="30%"><?php echo $product['p_name']; ?></td>
            <td width="30%"><?php echo $product['c_name']; ?></td>
            <td width="25%">
                <?php if($same){ ?>
                    <button data-id="<?php echo $product['p_id']; ?>" data-del="true" type="button" class="btn btn-danger btn-sm">Not same</button>
                <?php } else { ?>
                    <button data-id="<?php echo $product['p_id']; ?>" data-del="false" type="button" class="btn btn-dark btn-sm">Same Product</button>
                <?php } ?>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>