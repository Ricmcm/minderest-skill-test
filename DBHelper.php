<?php

namespace Minderest\SkillTest;

use Minderest\SkillTest\MinderestConfigurationManager;
use Exception;

/**
 * Class DBHelper
 * @package Minderest\SkillTest
 */
class DBHelper
{
    /**
     * @var
     */
    private static $instance;
    /**
     * @var
     */
    private $conn;

    /**
     * DBHelper constructor.
     * @param $conn
     */
    private function __construct($conn){
        $this->conn = $conn;
    }

    /**
     * @return DBHelper
     */
    public static function getInstance(){
        if(is_null(self::$instance)) {
            self::$instance = new DBHelper(self::init());
        }

        return self::$instance;
    }

    /**
     * @return string
     */
    public static function getSqlScript(){
        $mcm = MinderestConfigurationManager::getInstance();

        return
             "
                CREATE DATABASE IF NOT EXISTS `" .$mcm->getDatabase(). "`;
                USE `" . $mcm->getDatabase() . "`;

                DROP TABLE IF EXISTS `customer`;
                CREATE TABLE  `customer` (
                          `id` INT(11) UNSIGNED AUTO_INCREMENT,
                          `name` VARCHAR(150) NOT NULL,
                          `code` VARCHAR(25) UNIQUE NOT NULL,
                          PRIMARY KEY (`id`) )
                        ENGINE = INNODB DEFAULT CHARSET=utf8;
                
                DROP TABLE IF EXISTS `product`;
                CREATE TABLE `product` (
                          `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                          `name` VARCHAR(255) NOT NULL,
                          `customer_id` INT(11) UNSIGNED NOT NULL,
                          PRIMARY KEY (`id`),
                          UNIQUE KEY `UNIQUE` (`name`,`customer_id`),
                          KEY `FK_CUSTOMER_ID` (`customer_id`),
                          CONSTRAINT `FK_CUSTOMER_ID` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=INNODB DEFAULT CHARSET=utf8;
                
                DROP TABLE IF EXISTS `product_relationship`;
                CREATE TABLE `product_relationship` (
                        `product_id_from` INT(11) UNSIGNED NOT NULL,
                        `product_id_to` INT(11) UNSIGNED NOT NULL,
                        UNIQUE KEY `UNIQUE_INDEX` (`product_id_from`,`product_id_to`), KEY `FK_PRODUCT_ID_TO` (`product_id_to`),
                        CONSTRAINT `FK_PRODUCT_ID_FROM` FOREIGN KEY (`product_id_from`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                        CONSTRAINT `FK_PRODUCT_ID_TO` FOREIGN KEY (`product_id_to`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
                        ) ENGINE=INNODB DEFAULT CHARSET=utf8;" . PHP_EOL

                . "DELIMITER $$

                DROP TRIGGER IF EXISTS `ins_product_relationship`$$
                
                CREATE
                    TRIGGER `ins_product_relationship` BEFORE INSERT ON `product_relationship` 
                    FOR EACH ROW                 
                    BEGIN
                        IF(new.product_id_from = new.product_id_to) THEN
                            SIGNAL SQLSTATE '45000'
                            SET MESSAGE_TEXT = 'Is already same product';
                        END IF;
                        SELECT MAX(customer_id) INTO @customer_id_from FROM product WHERE id=new.product_id_from LIMIT 1;
                        SELECT MAX(customer_id) INTO @customer_id_to FROM product WHERE id=new.product_id_to LIMIT 1;
                        IF(@customer_id_from = @customer_id_to) THEN
                            SIGNAL SQLSTATE '45000'
                            SET MESSAGE_TEXT = 'Same client for product relationship is not allowed';
                        END IF;
                    END;" . PHP_EOL
                ."$$" . PHP_EOL . "DELIMITER ;" . PHP_EOL;
    }

    /**
     * @return \mysqli
     */
    private static function init(){
        $mcm = MinderestConfigurationManager::getInstance();

        // DB Connection
        $conn = new \mysqli($mcm->getHost(), $mcm->getUser(), $mcm->getPassword());
        if ($conn->connect_error) {
            die("Connection failed: " . $conn->connect_error);
        }

        // Create and Select Database
        if ( $conn->select_db($mcm->getDatabase()) === FALSE ){
            if ($conn->query(self::getSqlScript()) === FALSE ){
                die("Error creating database " . $mcm->getDatabase() . PHP_EOL);
            } else if ( $conn->select_db($mcm->getDatabase()) === FALSE ){
                die("Error selecting database " . $mcm->getDatabase() . PHP_EOL);
            }
        }

        return $conn;
    }

    /**
     * @return bool
     */
    public function close(){
        if(is_null($this->conn)) return true;
         return $this->conn->close();
    }

    /**
     * @param $customerId
     * @param $name
     * @return bool
     * @throws Exception
     */
    public function insertProduct($customerId, $name){
        $sql = "INSERT INTO `product` (`customer_id`, `name`) VALUES ($customerId,\"$name.\");";

        if ($this->conn->query($sql) !== TRUE) {
            throw new Exception("Error inserting product. " . $this->conn->error);
        }

        $this->close();
        return true;
    }

    /**
     * @param $productIdFrom
     * @param $customerIdFrom
     * @param $productIdTo
     * @param $customerIdTo
     * @return bool
     * @throws Exception
     */
    public function insertProductRelationship($productIdFrom, $productIdTo){
        $sql = "INSERT INTO `product_relationship` (`product_id_from`, `product_id_to`) 
                    VALUES ($productIdFrom, $productIdTo)";

        if ($this->conn->query($sql) !== TRUE) {
            throw new Exception("Error inserting product_relationship. " . $this->conn->error);
        }

        $this->close();
        return true;
    }

    /**
     * @param $productIdFrom
     * @param $customerIdFrom
     * @param $productIdTo
     * @param $customerIdTo
     * @return bool
     * @throws Exception
     */
    public function deleteProductRelationship($productIdFrom, $productIdTo){
        $sql = "DELETE FROM `product_relationship` WHERE `product_id_from`=$productIdFrom AND `product_id_to`=$productIdTo";

        if ($this->conn->query($sql) !== TRUE) {
            throw new Exception("Error inserting product_relationship. " . $this->conn->error);
        }

        $this->close();
        return true;
    }

    /**
     * @param $productId
     * @param $columns
     * @return bool
     * @throws Exception
     */
    public function updateProduct($productId, $columns){
        unset($columns['id']);
        $sql = "UPDATE `product` SET ";
        foreach($columns as $key => $value) {
            $sql .= "$key=$value";
        }
        $sql .= " WHERE `id`=$productId";

        if ($this->conn->query($sql) !== TRUE) {
            throw new Exception("Error updating product. " . $this->conn->error);
        }

        $this->close();
        return true;
    }

    /**
     * @param $table
     * @param $where
     * @return array
     */
    public function query($table, $where){
        $sql = "SELECT * FROM `$table` WHERE $where";
        $result = $this->conn->query($sql);

        $rows = [];
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $rows[]=$row;
            }
            $result->close();
        }

        return $rows;
    }

    /**
     * @param $name
     * @return array
     */
    public function searchCustomers($name){
        $sql = "SELECT * FROM `customer` WHERE `name` LIKE '%$name%'";

        $result = $this->conn->query($sql);
        $toReturn = array();

        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $toReturn[$row['id']] = $row['name'];
            }
        }

        return $toReturn;
    }

    /**
     * @param $customerId
     * @param $name
     * @param int $offset
     * @param int $limit
     * @return array
     * @throws Exception
     */
    public function searchProductByName($customerId, $name, $offset = 0, $limit = 25){
        $name = addslashes($name);
        $sql = "SELECT p.id AS p_id, p.name AS p_name, GROUP_CONCAT(pr.product_id_from SEPARATOR ',') AS p_product_id, c.name AS c_name
                FROM `product` p 
                LEFT JOIN `customer` c ON c.`id` = p.`customer_id`
                LEFT JOIN `product_relationship` pr ON pr.`product_id_to` = p.`id`
                WHERE p.`customer_id`!=$customerId AND p.`name` LIKE '%$name%'
                GROUP BY p.id";
        if($limit>0) $sql .= " LIMIT $limit";
        if($offset>0) $sql .= " OFFSET $offset";

        if(($result = $this->conn->query($sql)) === FALSE ) {
            throw new Exception("Error searching products by name. " . $this->conn->error);
        }

        $toReturn = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $toReturn[] = $row;
            }
        }

        return $toReturn;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function findAllCustomers(){
        $sql = "SELECT * from `customer`";
        if(($result = $this->conn->query($sql)) === FALSE ) {
            throw new Exception("Error finding customers. " . $this->conn->error);
        }

        $toReturn = array();
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $toReturn[$row['id']] = $row['name'];
            }
        }

        return $toReturn;
    }

    /**
     * @param $name
     * @param $code
     * @return bool
     * @throws Exception
     */
    public function newCustomer($name, $code){
        $sql = "INSERT INTO `customer` (`name`, `code`) VALUES (\"$name\", \" . $code . \")";
        if ($this->conn->query($sql) !== TRUE) {
            throw new Exception("Error inserting customer. " . $this->conn->error);
        }

        $this->close();
        return true;
    }
}