# Mindterst Skill Test

##### Prueba de programación para desarrollador PHP/BBDD
###### Minderest S.L. --v1.2

**App:** http://mstric.duckdns.org  
**Source Code:**
https://bitbucket.org/Ricmcm/minderest-skill-test  

## Conexión con la base de datos
Se incluye un fichero de configuración en el que indicar los parámetros de conexión al servidor de bases de datos.
```
config.ini
```
  
## Script BBDD
```
load-fixtures.php
```
Incluye un menu de opciones:

```
$ php load-fixtures.php
Minderest SkillTest Utils
===========================
[1] - Load test customers
[2] - Generate SQL Script
[0] - Exit
Option:

```
**[1] - Load test customers** - Solicita la cantidad de clientes a generar y realiza las inserciones en la base de datos  
**[2] - Generate SQL Script** - Genera el Script SQL necesario para crear la base de datos, tablas y relaciones,
así como la inserción de los clientes.    

Se incluye un Script SQL ya generado con 10 clientes.
```
mstric.sql
```

## Descripción del esquema de bases de datos
Está compuesto de tres tablas:  
**customer** - Clientes. Id y nombre  
**product** - Productos. Id, nombre y cliente al que pertenece.  
**product_relationship** - Implementa la relación de igualdad entre productos.  
Las restricciones indicadas en el enunciado están controladas a través de un *trigger before insert*.  

![Entity Relation Diagram](public/img/er_diagram.png)  
    
## Aplicación PHP
Está desplegada en http://mstric.duckdns.org  

  
Consta de dos aparados:
#### Add Products
Formulario para añadir productos indicando el cliente asociado y su nombre.

#### Categorize Products
Pantalla para consignar la igualdad entre productos. Un formulario en la parte superior en el que se selecciona el cliente, 
el producto y campo de texto para buscar productos potencialmente iguales en otros clientes.
El resultado se muestra en dos tablas que contienen la misma información.  
Una utilizando **datatables.js** y la otra **PHP**.  
  
En cada fila de la tabla (producto similar en otro cliente) se muestra un botón para indicar la igualdad entre dicho
producto y el producto seleccionado en el formulario inicial.  
Si esta relación de igualdad ya ha sido establecida con anterioridad se indica adecuadamente y se aparece un botón para eliminar dicha relación.

