<?php

require "../autoload.php";

use Minderest\SkillTest\DBHelper as DBHelper;

$em = DBHelper::getInstance();
$customers = $em->findAllCustomers();

?>

<h4>Categorize Product</h4>

<form action="#" method="POST" data-action="searchProducts" class="needs-validation" novalidate>
    <div class="row">
        <div class="col-md-3 mb-3">
            <label for="customer_id">Customer</label>
            <select name="search[customer_id]" class="custom-select d-block w-100" id="customer_id" required="">
                <option value="">Choose...</option>
                <?php foreach($customers as $id => $customer){ ?>
                    <option value="<?php echo $id; ?>"><?php echo $customer; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-3 mb-3">
            <label for="product_id">Customer Product</label>
            <select disabled name="search[product_id]" class="custom-select d-block w-100" id="product_id" required="">
                <option value="">Choose...</option>
            </select>
        </div>
        <div class="col-md-6 mb-3">
            <label for="search">Product Name</label>
            <input disabled type="text" class="form-control" id="search" placeholder="Start typing for search" value="" required="">
        </div>
    </div>
</form>

<div id="cp-container" class="row mt-10" style="display:none;">
    <div class="col-md-6">
        <h5>DataTables</h5>
        <div id="dt-results" class="row">
            <table id="table-dt" class="display" style="width:100%">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Product Name</th>
                    <th scope="col">Customer</th>
                    <th scope="col">Actions</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
    <div class="col-md-6">
        <h5>PHP</h5>
        <div id="results" class="row">
        </div>
    </div>
</div>

<script>
    function isSameProduct(data){
        var same = false;
        if(data.p_product_id != null){
            $.each((data.p_product_id+"").split(","), function(){
                if(product.val() == this) { same = true; return; }
            });
        }
        return same;
    }

    var searchText = $("#search");
    var customer = $("#customer_id");
    var product = $("#product_id");
    var dt = $("#dt");
    customer.change(function(){refreshProducts($(this).val())});
    product.change(function(){
        searchText.val("");
        if($(this).val()>0) searchText.prop("disabled",false);
        else searchText.prop("disabled",true);
    });
    searchText.keyup(function (){
        if($(this).val().length>=3){
            searchProducts(customer.val(), searchText.val(), 1);
        } else {
            $("#cp-container").hide();
        }
    });
    var dt = $("#table-dt").DataTable({
        "order": [],
        "columns": [
            { data: "p_id" },
            { data: "p_name" },
            { data: "c_name" },
            {
                data: null,
                render: function ( data, type, row ) {
                    return isSameProduct(data) ?  "<button data-id='"+data.p_id+"' data-del='true' type='button' class='btn btn-danger btn-sm'>Not same</button>"
                            : "<button data-id='"+data.p_id+"' data-del='false' type='button' class='btn btn-dark btn-sm'>Assign Product</button>";
                }
            }
        ],
        createdRow: function(row, data){
            if(isSameProduct(data))$(row).addClass("tr-success");
        }
    });

</script>

